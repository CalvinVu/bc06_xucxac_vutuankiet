import logo from "./logo.svg";
import "./App.css";
import XucXacPage from "./XucXacPage/XucXacPage/XucXacPage";

function App() {
  return (
    <div className="App">
      <XucXacPage />
    </div>
  );
}

export default App;
