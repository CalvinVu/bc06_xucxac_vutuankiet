import React, { useState } from "react";
import bgGame from "../../assist/bgGame.png";
import "../bgGame.css";
import PathPlay from "./PathPlay";
import PathResult from "./PathResult";

export default function XucXacPage() {
  const [xucXacArr, setXucXacArr] = useState([
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
  ]);

  let playXucXac = () => {
    let tongDiem = 0;
    let newXucXacArr = xucXacArr.map((item) => {
      let number = Math.floor(Math.random() * 6) + 1;
      tongDiem += number;
      console.log(tongDiem);
      return {
        img: `./imgXucSac/${number}.png`,
        giaTri: number,
      };
    });
    setXucXacArr(newXucXacArr);
    luaChon != null && setLuotChoi(luotChoi + 1);
    ((luaChon == "TAI" && tongDiem >= 11) ||
      (luaChon == "XIU" && tongDiem < 11)) &&
      setSoBanThang(soBanThang + 1);
    if (luaChon == null) {
      setKetQua(null);
    } else if (
      (luaChon == "TAI" && tongDiem >= 11) ||
      (luaChon == "XIU" && tongDiem < 11)
    ) {
      setKetQua("BAN DA CHIEN THANG");
    } else {
      setKetQua("BAN DA THUA");
    }
  };
  const [luaChon, setLuaChon] = useState(null);
  let renderLuaChon = (value) => {
    setLuaChon(value);
  };

  const [luotChoi, setLuotChoi] = useState(0);
  const [soBanThang, setSoBanThang] = useState(0);
  const [ketQua, setKetQua] = useState(null);

  return (
    <div className="bgGame" style={{ backgroundImage: `url(${bgGame})` }}>
      XucXacPage
      <div>
        <PathPlay xucXacArr={xucXacArr} renderLuaChon={renderLuaChon} />
      </div>
      <div style={{ margin: "80px" }}>
        <PathResult
          luaChon={luaChon}
          playXucXac={playXucXac}
          luotChoi={luotChoi}
          soBanThang={soBanThang}
          ketQua={ketQua}
          xucXacArr={xucXacArr}
        />
      </div>
    </div>
  );
}
