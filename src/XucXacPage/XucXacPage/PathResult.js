import React from "react";

export default function PathResult({
  luaChon,
  playXucXac,
  luotChoi,
  soBanThang,
  ketQua,
  xucXacArr,
}) {
  let renderSoDiem = () => {
    let soDiem = 0;
    xucXacArr.map((item) => {
      soDiem += item.giaTri;
    });
    return soDiem;
  };
  return (
    <div>
      <div>
        <button className="p-3" onClick={playXucXac}>
          PLAY
        </button>
      </div>
      <h2>BAN CHON: {luaChon}</h2>
      <h2>TONG SO DIEM LA: {renderSoDiem()}</h2>
      <h2>LUOT CHOI: {luotChoi}</h2>
      <h2>SO BAN THANG: {soBanThang}</h2>
      <h2 className="text-danger">KET QUA: {ketQua}</h2>
    </div>
  );
}
