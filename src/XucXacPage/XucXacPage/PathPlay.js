import React from "react";

export default function PathPlay({ xucXacArr, renderLuaChon }) {
  let renderXucXacArr = () => {
    return xucXacArr.map((item) => {
      return (
        <img style={{ width: "150px", margin: "5px" }} src={item.img}></img>
      );
    });
  };
  return (
    <div className="container d-flex justify-content-between">
      <div>
        <button
          className="btn btn-warning p-5"
          onClick={() => {
            renderLuaChon("TAI");
          }}
        >
          TAI
        </button>
      </div>
      <div>{renderXucXacArr()}</div>
      <div>
        <button
          className="btn btn-success p-5"
          onClick={() => {
            renderLuaChon("XIU");
          }}
        >
          XIU
        </button>
      </div>
    </div>
  );
}
